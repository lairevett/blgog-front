import { STATUS_INITIALIZED, COMMENT_RETRIEVE_MULTIPLE } from '../utils/Types';

export const COMMENT_INITIAL_STATE_MULTIPLE = {
    count: 0,
    next: null,
    previous: null,
    results: [],
    status: STATUS_INITIALIZED
};

const initialState = {
    comments: COMMENT_INITIAL_STATE_MULTIPLE
};

export default (state = initialState, action) => {
    switch (action.type) {
        case COMMENT_RETRIEVE_MULTIPLE:
            return {
                comments: action.payload
            };
        default:
            return state;
    }
};
