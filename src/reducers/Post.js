import {
    STATUS_INITIALIZED,
    POST_RETRIEVE_MULTIPLE,
    POST_RETRIEVE_ONE
} from '../utils/Types';

export const POST_INITIAL_STATE_MULTIPLE = {
    count: 0,
    next: null,
    previous: null,
    results: [],
    status: STATUS_INITIALIZED
};

export const POST_INITIAL_STATE_ONE = {
    id: -1,
    url: '',
    user: '',
    title: '',
    content: '',
    comments: [],
    created_at: '',
    updated_at: '',
    status: STATUS_INITIALIZED
};

const initialState = {
    posts: POST_INITIAL_STATE_MULTIPLE,
    post: POST_INITIAL_STATE_ONE
};

export default (state = initialState, action) => {
    switch (action.type) {
        case POST_RETRIEVE_MULTIPLE:
            return {
                ...state,
                posts: action.payload
            };
        case POST_RETRIEVE_ONE:
            return {
                ...state,
                post: action.payload
            };
        default:
            return state;
    }
};
