import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';
import { reducer as formReducer } from 'redux-form';
import postReducer from './Post';
import commentReducer from './Comment';
import userReducer from './User';

// additional reducers
const reducers = {
    form: formReducer,
    post: postReducer,
    comment: commentReducer,
    user: userReducer
};

// create root reducer with combined reducers
export default history =>
    combineReducers({
        router: connectRouter(history), // react-redux router reducer
        ...reducers // additional reducers
    });
