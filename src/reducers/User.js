import {
    STATUS_INITIALIZED,
    USER_RETRIEVE_ONE_USERNAME,
    USER_RETRIEVE_ONE_ID,
    USER_RETRIEVE_POST_AUTHOR,
    USER_RETRIEVE_POSTS_AUTHORS,
    USER_RETRIEVE_COMMENTS_AUTHORS
} from '../utils/Types';

export const USER_INITIAL_STATE_ONE = {
    id: -1,
    url: '',
    username: '',
    email: '',
    first_name: '',
    last_name: '',
    avatar: '',
    posts: [],
    comments: [],
    is_staff: false,
    date_joined: '',
    date_updated: '',
    status: STATUS_INITIALIZED
};

// API seems to always return user list instead of user instance when browsing /api/posts/:id/user/
export const USER_INITIAL_STATE_POST_AUTHOR = {
    count: 0,
    next: null,
    previous: null,
    results: [],
    status: STATUS_INITIALIZED
};

export const USER_INITIAL_STATE_POSTS_AUTHORS = [];

export const USER_INITIAL_STATE_COMMENTS_AUTHORS = [];

const initialState = {
    user: USER_INITIAL_STATE_ONE,
    user_username: USER_INITIAL_STATE_ONE,
    post: USER_INITIAL_STATE_POST_AUTHOR,
    posts: USER_INITIAL_STATE_POSTS_AUTHORS,
    comments: USER_INITIAL_STATE_COMMENTS_AUTHORS
};

export default (state = initialState, action) => {
    switch (action.type) {
        case USER_RETRIEVE_ONE_USERNAME:
            return {
                ...state,
                user_username: action.payload
            };
        case USER_RETRIEVE_ONE_ID:
            return {
                ...state,
                user: action.payload
            };
        case USER_RETRIEVE_POST_AUTHOR:
            return {
                ...state,
                post: action.payload
            };
        case USER_RETRIEVE_POSTS_AUTHORS:
            return {
                ...state,
                posts: action.payload
            };
        case USER_RETRIEVE_COMMENTS_AUTHORS:
            return {
                ...state,
                comments: action.payload
            };
        default:
            return state;
    }
};
