import axios from 'axios';
import {
    STATUS_RETRIEVED,
    STATUS_NO_RESULTS,
    STATUS_LOADING,
    STATUS_NOT_FOUND,
    STATUS_NETWORK_ERROR
} from '../utils/Types';

/**
 * Generates dispatch with new type, initial state and status.
 */
const generateDispatch = (dispatch, type, state, status) => {
    dispatch({
        type,
        payload: {
            ...state,
            status
        }
    });
};

/**
 * Generates dispatch with new type, initial state but without status.
 */
const generateUncontrolledDispatch = (dispatch, type, state) => {
    dispatch({
        type,
        payload: state
    });
};

/**
 * Sets the redux status state to loading, tries to retrieve the response, then
 * requests resource by GET response from the API with axios, if there is
 * response but it's empty, sets the redux status state to not found, if there
 * is no response completely sets the redux status state to network error,
 * otherwise returns the response.
 */
export const tryToRetrieveResponse = async (
    dispatch,
    type,
    initialState,
    resource,
    controlled
) => {
    // let the user know what's happening
    controlled &&
        generateDispatch(dispatch, type, initialState, STATUS_LOADING);

    try {
        // try returning the actual data from resource
        return await axios.get(resource);
    } catch (e) {
        // in case of not found or network error dispatch not found or network error status + log to console
        console.log(
            `Cannot retrieve resources from the API! Received message: "${
                e.message
            }".`
        );

        if (e.response) {
            // request was made, but response falls out of 2xx status code range, probably 404 not found
            controlled &&
                generateDispatch(
                    dispatch,
                    type,
                    initialState,
                    STATUS_NOT_FOUND
                );
        } else if (e.request) {
            // request was made but no response was retrieved, probably some kind of network error
            controlled &&
                generateDispatch(
                    dispatch,
                    type,
                    initialState,
                    STATUS_NETWORK_ERROR
                );
        } else {
            // this should not happen but
            // TODO: add handler
        }

        return null;
    }
};

/**
 * Tries to retrieve and return more than one response from provided resources
 * array.
 */
const tryToRetrieveResponses = (dispatch, type, resources, initialState) => {
    const responses = [];

    resources.forEach(resource => {
        responses.push(
            tryToRetrieveResponse(dispatch, type, initialState, resource, false)
        );
    });

    return responses;
};

/**
 * Retrieves one item from API through axios GET request.
 */
export const retrieveOne = (type, resource, initialState) => async dispatch => {
    const response = await tryToRetrieveResponse(
        dispatch,
        type,
        initialState,
        resource,
        true
    );

    response &&
        generateDispatch(dispatch, type, response.data, STATUS_RETRIEVED);
};

/**
 * Retrieves multiple items from API through axios GET request.
 */
export const retrieveMultiple = (
    type,
    resource,
    initialState
) => async dispatch => {
    const response = await tryToRetrieveResponse(
        dispatch,
        type,
        initialState,
        resource,
        true
    );

    // if there are more than 0 posts, return all of them with status retrieved
    response && response.data.count > 0
        ? generateDispatch(dispatch, type, response.data, STATUS_RETRIEVED)
        : // if there aren't any posts return initial state with status no results
          generateDispatch(dispatch, type, initialState, STATUS_NO_RESULTS);
};

/**
 * Dispatches data to redux state from all of the responses.
 */
export const retrieveMultipleArray = (
    type,
    resources,
    initialState
) => async dispatch => {
    let responses = tryToRetrieveResponses(
        dispatch,
        type,
        resources,
        initialState
    );

    responses = await Promise.all(responses);

    const data = [];

    responses.forEach(response => {
        data.push(response.data);
    });

    // because it responds with object type instead of an array if status is present
    generateUncontrolledDispatch(dispatch, type, data);
};

/**
 * Simple function that concatenates resource url with id and returns it.
 */
export const generateResourceUrlFromId = (url, id) => url + id + '/';
