import { COMMENT_RETRIEVE_MULTIPLE } from '../utils/Types';
import { getCommentsResourceFromPostId } from '../utils/API';
import { COMMENT_INITIAL_STATE_MULTIPLE } from '../reducers/Comment';
import { retrieveMultiple, tryToRetrieveResponse } from './Generics';
import { retrieveUsersFromCommentIds } from './User';

// TODO: clean this mess
export const retrieveCommentsWithAuthors = postId => async dispatch => {
    const comments = await tryToRetrieveResponse(
        dispatch,
        COMMENT_RETRIEVE_MULTIPLE,
        COMMENT_INITIAL_STATE_MULTIPLE,
        getCommentsResourceFromPostId(postId),
        true
    );

    if (comments) {
        const commentIds = [];
        comments.data.results.forEach(comment => {
            commentIds.push(comment.id);
        });

        dispatch(
            retrieveMultiple(
                COMMENT_RETRIEVE_MULTIPLE,
                getCommentsResourceFromPostId(postId),
                COMMENT_INITIAL_STATE_MULTIPLE
            )
        );

        dispatch(retrieveUsersFromCommentIds(commentIds));
    }
};
