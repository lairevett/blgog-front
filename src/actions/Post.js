import { POST_RESOURCE, getPostResourceFromId } from '../utils/API';
import { POST_RETRIEVE_MULTIPLE, POST_RETRIEVE_ONE } from '../utils/Types';
import {
    POST_INITIAL_STATE_MULTIPLE,
    POST_INITIAL_STATE_ONE
} from '../reducers/Post';
import {
    tryToRetrieveResponse,
    retrieveMultiple,
    retrieveOne
} from './Generics';
import { retrieveUserFromPostId, retrieveUsersFromPostIds } from './User';

export const retrievePostWithAuthor = id => dispatch => {
    dispatch(
        retrieveOne(
            POST_RETRIEVE_ONE,
            getPostResourceFromId(id),
            POST_INITIAL_STATE_ONE
        )
    );

    dispatch(retrieveUserFromPostId(id));
};

// TODO: clean this mess
export const retrievePostsWithAuthors = () => async dispatch => {
    const posts = await tryToRetrieveResponse(
        dispatch,
        POST_RETRIEVE_MULTIPLE,
        POST_INITIAL_STATE_MULTIPLE,
        POST_RESOURCE,
        true
    );

    if (posts) {
        const postIds = [];
        posts.data.results.forEach(post => postIds.push(post.id));

        dispatch(
            retrieveMultiple(
                POST_RETRIEVE_MULTIPLE,
                POST_RESOURCE,
                POST_INITIAL_STATE_MULTIPLE
            )
        );

        dispatch(retrieveUsersFromPostIds(postIds));
    }
};
