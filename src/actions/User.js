import { retrieveOne, retrieveMultipleArray } from './Generics';
import {
    USER_RETRIEVE_ONE_USERNAME,
    USER_RETRIEVE_ONE_ID,
    USER_RETRIEVE_POST_AUTHOR,
    USER_RETRIEVE_POSTS_AUTHORS,
    USER_RETRIEVE_COMMENTS_AUTHORS
} from '../utils/Types';
import {
    getUserResourceFromId,
    getUserResourceFromPostId,
    getUserResourcesFromPostIds,
    getUserResourcesFromCommentIds,
    getUserResourceFromUsername
} from '../utils/API';
import {
    USER_INITIAL_STATE_ONE,
    USER_INITIAL_STATE_POST_AUTHOR,
    USER_INITIAL_STATE_POSTS_AUTHORS,
    USER_INITIAL_STATE_COMMENTS_AUTHORS
} from '../reducers/User';

export const retrieveUserFromUsername = username => dispatch => {
    dispatch(
        retrieveOne(
            USER_RETRIEVE_ONE_USERNAME,
            getUserResourceFromUsername(username),
            USER_INITIAL_STATE_ONE
        )
    );
};

export const retrieveUserFromId = id => dispatch => {
    dispatch(
        retrieveOne(
            USER_RETRIEVE_ONE_ID,
            getUserResourceFromId(id),
            USER_INITIAL_STATE_ONE
        )
    );
};

export const retrieveUserFromPostId = postId => dispatch => {
    dispatch(
        retrieveOne(
            USER_RETRIEVE_POST_AUTHOR,
            getUserResourceFromPostId(postId),
            USER_INITIAL_STATE_POST_AUTHOR
        )
    );
};

export const retrieveUsersFromPostIds = postIds => dispatch => {
    dispatch(
        retrieveMultipleArray(
            USER_RETRIEVE_POSTS_AUTHORS,
            getUserResourcesFromPostIds(postIds),
            USER_INITIAL_STATE_POSTS_AUTHORS
        )
    );
};

export const retrieveUsersFromCommentIds = commentIds => dispatch => {
    dispatch(
        retrieveMultipleArray(
            USER_RETRIEVE_COMMENTS_AUTHORS,
            getUserResourcesFromCommentIds(commentIds),
            USER_INITIAL_STATE_COMMENTS_AUTHORS
        )
    );
};
