import styled from 'styled-components';

export const StyledTable = styled.table`
    border: 0;
    display: block;
    overflow-x: auto;
`;

export const StyledTh = styled.th`
    padding: 1rem;
`;

export const StyledTd = styled.td`
    padding: 1rem;
    text-align: center;
`;
