import React from 'react';

export const Container = ({ children }) => (
    <article className="col-xs-12 col-sm-8 col-md-8 col-lg-10">
        {children}
    </article>
);

export const Header = ({ children, small }) => (
    <header className="center-xs start-sm start-md start-lg">
        {small ? <h3>{children}</h3> : <h2>{children}</h2>}
    </header>
);

export const Body = ({ children }) => (
    <section className="center-xs start-sm start-md start-lg col-sm-offset-1 col-md-offset-1 col-lg-offset-1">
        {children}
    </section>
);

export const Footer = ({ children }) => (
    <footer className="center-xs start-sm start-md start-lg">
        <p>{children}</p>
    </footer>
);
