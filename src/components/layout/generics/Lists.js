import styled from 'styled-components';

export const UnorderedList = styled.ul`
    list-style-type: none;
`;

export const ListItem = styled.ul`
    .label {
        color: #33ff00;
    }
`;
