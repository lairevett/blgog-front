import React from 'react';

export const renderField = ({
    input,
    label,
    type,
    placeholder,
    meta: { touched, error }
}) => (
    <fieldset>
        <label>{label}</label>
        <input {...input} placeholder={placeholder} type={type} />
        {touched && error && <span className="error">{error}</span>}
    </fieldset>
);
