import React from 'react';
import styled from 'styled-components';

const StyledHeader = styled.header`
    margin: 2.5vh 0;
    h1 {
        color: #33ff00;
        font-weight: normal;
        font-size: 3.5rem;
    }
`;

export default ({ title }) => (
    <StyledHeader className="row center-xs center-sm center-md center-lg">
        <h1>{title}</h1>
    </StyledHeader>
);
