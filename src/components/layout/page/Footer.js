import React from 'react';
import styled from 'styled-components';

const StyledFooter = styled.footer`
    margin: 2.5vh 0;
    em {
        font-size: 1.2rem;
        font-style: normal;
    }
`;

export default ({ currentYear }) => (
    <StyledFooter className="row center-xs end-sm end-md end-lg">
        <em>
            Copyright &copy; {currentYear}{' '}
            <a
                href="https://github.com/lairevett"
                target="_blank"
                rel="noopener noreferrer"
            >
                Marcin Sadowski
            </a>
        </em>
    </StyledFooter>
);
