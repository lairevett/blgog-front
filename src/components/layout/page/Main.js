import React from 'react';
import styled from 'styled-components';

const StyledMain = styled.main`
    margin: 2.5vh 0;
    article {
        &:not(:last-child) {
            margin-bottom: 1.5vh;
        }
        header {
            h2 {
                font-weight: normal;
                margin-bottom: 0.5vh;
                font-size: 1.8rem;
            }
            h3 {
                font-weight: normal;
                font-size: 1.35rem;
            }
        }
        section {
            font-size: 1.3rem;
        }
        footer {
            margin-top: 0.5vh;
            font-size: 1.2rem;
        }
    }
`;

export default ({ children }) => (
    <StyledMain className="row center-xs center-sm center-md center-lg">
        {children}
    </StyledMain>
);
