import React from 'react';

export default ({ children }) => (
    <section className="container container-fluid">{children}</section>
);
