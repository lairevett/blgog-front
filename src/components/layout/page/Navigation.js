import React from 'react';
import styled from 'styled-components';
import { NavLink } from 'react-router-dom';
import { routes } from '../../routes/Routes';

const StyledNav = styled.nav`
    margin: 2.5vh 0;
    ul {
        list-style-type: none;
        li {
            font-size: 1.4rem;
        }
    }
`;

export default () => (
    <StyledNav className="row center-xs center-sm center-md center-lg">
        <ul className="row col-xs-6 col-sm-9 col-md-9 col-lg-12">
            <li className="col-xs">
                <NavLink exact activeClassName="active" to={routes.home.path}>
                    {routes.home.key}
                </NavLink>
            </li>
            <li className="col-xs">
                <NavLink activeClassName="active" to={routes.archive.path}>
                    {routes.archive.key}
                </NavLink>
            </li>
            <li className="col-xs">
                <NavLink activeClassName="active" to={routes.about.path}>
                    {routes.about.key}
                </NavLink>
            </li>
            <li className="col-xs">
                <NavLink activeClassName="active" to={routes.signIn.path}>
                    {routes.signIn.key}
                </NavLink>
            </li>
            <li className="col-xs">
                <NavLink activeClassName="active" to={routes.signUp.path}>
                    {routes.signUp.key}
                </NavLink>
            </li>
        </ul>
    </StyledNav>
);
