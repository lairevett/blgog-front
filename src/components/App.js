import React from 'react';
import { ConnectedRouter } from 'connected-react-router';
import Container from './layout/page/Container';
import Header from './layout/page/Header';
import Navigation from './layout/page/Navigation';
import Main from './layout/page/Main';
import Footer from './layout/page/Footer';
import Routes from './routes/Routes';

export default ({ history }) => (
    <ConnectedRouter history={history}>
        <Container>
            <Header title="Good Old Games" />
            <Navigation />
            <Main>
                <Routes />
            </Main>
            <Footer currentYear={new Date().getFullYear()} />
        </Container>
    </ConnectedRouter>
);
