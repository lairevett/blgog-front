import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { retrievePostsWithAuthors } from '../../../actions/Post';
import PostList from '../../app/post/PostList';

class Home extends PureComponent {
    static propTypes = {
        retrievePostsWithAuthors: PropTypes.func.isRequired,
        posts: PropTypes.shape({
            count: PropTypes.number.isRequired,
            next: PropTypes.string,
            previous: PropTypes.string,
            results: PropTypes.arrayOf(
                PropTypes.shape({
                    id: PropTypes.number.isRequired,
                    url: PropTypes.string.isRequired,
                    user: PropTypes.string.isRequired,
                    title: PropTypes.string.isRequired,
                    content: PropTypes.string.isRequired,
                    comments: PropTypes.arrayOf(PropTypes.string.isRequired),
                    created_at: PropTypes.string.isRequired,
                    updated_at: PropTypes.string.isRequired
                })
            ),
            status: PropTypes.string.isRequired
        }),
        authors: PropTypes.arrayOf(
            PropTypes.shape({
                count: PropTypes.number.isRequired,
                next: PropTypes.string,
                previous: PropTypes.string,
                results: PropTypes.arrayOf(
                    PropTypes.shape({
                        id: PropTypes.number.isRequired,
                        url: PropTypes.string.isRequired,
                        username: PropTypes.string.isRequired,
                        email: PropTypes.string.isRequired,
                        first_name: PropTypes.string.isRequired,
                        last_name: PropTypes.string.isRequired,
                        avatar: PropTypes.string, // TODO: change to isRequired when the avatars are implemented
                        posts: PropTypes.array.isRequired,
                        comments: PropTypes.array.isRequired,
                        is_staff: PropTypes.bool.isRequired,
                        date_joined: PropTypes.string.isRequired,
                        date_updated: PropTypes.string.isRequired
                    })
                )
            })
        )
    };

    componentDidMount = () => {
        this.props.retrievePostsWithAuthors();
    };

    render = () => (
        <PostList posts={this.props.posts} authors={this.props.authors} />
    );
}

const mapStateToProps = state => ({
    posts: state.post.posts,
    authors: state.user.posts
});

const mapDispatchToProps = dispatch => ({
    retrievePostsWithAuthors: () => dispatch(retrievePostsWithAuthors())
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Home);
