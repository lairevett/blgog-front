import React, { PureComponent } from 'react';
import { Container, Header, Body } from '../../layout/generics/Content';
import SignInForm from '../../app/sign-in/SignInForm';

export default class SignIn extends PureComponent {
    // handleSubmit = e => {
    //     e.preventDefault();
    //     console.log('registered click!');
    //     console.log(e.target.username.value);
    // };

    render = () => (
        <Container>
            <Header>Sign in</Header>
            <Body>
                <SignInForm />
            </Body>
        </Container>
    );
}
