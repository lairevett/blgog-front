import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import Home from './home/Home';
import Archive from './archive/Archive';
import ArchiveDetails from './archive/ArchiveDetails';
import About from './about/About';
import AboutDetails from './about/AboutDetails';
import SignIn from './sign-in/SignIn';
import SignUp from './sign-up/SignUp';
import NotFound from '../app/generics/NotFound';

export const routes = {
    home: {
        key: 'Home',
        path: '/home',
        component: Home
    },
    archive: {
        key: 'Archive',
        path: '/archive',
        component: Archive
    },
    archiveDetails: {
        key: 'Archive details',
        path: '/archive/:id',
        component: ArchiveDetails
    },
    about: {
        key: 'About',
        path: '/about',
        component: About
    },
    aboutDetails: {
        key: 'About details',
        path: '/about/:id',
        component: AboutDetails
    },
    signIn: {
        key: 'Sign in',
        path: '/sign-in',
        component: SignIn
    },
    signUp: {
        key: 'Sign up',
        path: '/sign-up',
        component: SignUp
    }
};

export default () => (
    <Switch>
        <Redirect exact from="/" to={routes.home.path} />
        {Object.values(routes).map(route => (
            <Route
                key={route.key}
                exact
                path={route.path}
                component={route.component}
            />
        ))}
        <NotFound />
    </Switch>
);
