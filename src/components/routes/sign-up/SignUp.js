import React from 'react';
import { Container, Header, Body } from '../../layout/generics/Content';
import SignUpForm from '../../app/sign-up/SignUpForm';

export default () => (
    <Container>
        <Header>Sign up</Header>
        <Body>
            <SignUpForm />
        </Body>
    </Container>
);
