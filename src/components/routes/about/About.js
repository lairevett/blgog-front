import React from 'react';
import { Container, Header, Body } from '../../layout/generics/Content';
import { UnorderedList, ListItem } from '../../layout/generics/Lists';

export default () => (
    <Container>
        <Header>About this project</Header>
        <Body>
            <span className="active">BlGOG (Good Old Games Blog)</span> is a
            small project of mine made for apprenticeship to polish my
            JavaScript and React skills and in attempt to learn more about
            Python, Django and REST APIs. You can browse the source code of this
            app on my GitHub (link in the footer of page).
            <p>Tech used in the process of crafting this application:</p>
            <UnorderedList>
                <ListItem>
                    <a
                        href="https://github.com/facebook/react"
                        target="_blank"
                        rel="noopener noreferrer"
                    >
                        React
                    </a>
                </ListItem>
                <ListItem>
                    <a
                        href="https://github.com/reduxjs/redux"
                        target="_blank"
                        rel="noopener noreferrer"
                    >
                        Redux
                    </a>
                </ListItem>
                <ListItem>
                    <a
                        href="https://github.com/axios/axios"
                        target="_blank"
                        rel="noopener noreferrer"
                    >
                        Axios
                    </a>
                </ListItem>
                <ListItem>
                    <a
                        href="https://github.com/styled-components/styled-components"
                        target="_blank"
                        rel="noopener noreferrer"
                    >
                        Styled Components
                    </a>
                </ListItem>
                <ListItem>
                    <a
                        href="https://github.com/kristoferjoseph/flexboxgrid"
                        target="_blank"
                        rel="noopener noreferrer"
                    >
                        Flexbox Grid
                    </a>
                </ListItem>
                <ListItem>
                    <a
                        href="https://github.com/lairevett/blgog-front/blob/master/package.json"
                        target="_blank"
                        rel="noopener noreferrer"
                    >
                        ...more
                    </a>
                </ListItem>
            </UnorderedList>
        </Body>
    </Container>
);
