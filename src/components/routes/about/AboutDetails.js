import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { goBack } from 'connected-react-router';
import PropTypes from 'prop-types';
import { retrieveUserFromId } from '../../../actions/User';
import UserDetails from '../../app/user/UserDetails';

// TODO: enhance the look of this page a bit
class AboutDetails extends PureComponent {
    static propTypes = {
        retrieveUser: PropTypes.func.isRequired,
        goBack: PropTypes.func.isRequired,
        user: PropTypes.shape({
            id: PropTypes.number.isRequired,
            url: PropTypes.string.isRequired,
            username: PropTypes.string.isRequired,
            email: PropTypes.string.isRequired,
            avatar: PropTypes.string, // TODO: change to isRequired when the avatars are implemented
            posts: PropTypes.arrayOf(PropTypes.string.isRequired).isRequired,
            comments: PropTypes.arrayOf(PropTypes.string.isRequired).isRequired,
            is_staff: PropTypes.bool.isRequired,
            date_joined: PropTypes.string.isRequired,
            date_updated: PropTypes.string.isRequired,
            status: PropTypes.string.isRequired
        }).isRequired
    };

    componentDidMount = () => {
        this.props.retrieveUser();
    };

    render = () => (
        <UserDetails user={this.props.user} goBack={this.props.goBack} />
    );
}

const mapStateToProps = state => ({
    user: state.user.user
});

const mapDispatchToProps = (dispatch, ownProps) => ({
    goBack: () => dispatch(goBack()),
    retrieveUser: () => dispatch(retrieveUserFromId(ownProps.match.params.id))
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(AboutDetails);
