import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { retrievePostWithAuthor } from '../../../actions/Post';
import { retrieveCommentsWithAuthors } from '../../../actions/Comment';
import PostDetails from '../../app/post/PostDetails';
import CommentList from '../../app/comment/CommentList';

class ArchiveDetails extends PureComponent {
    static propTypes = {
        retrievePostWithAuthor: PropTypes.func.isRequired,
        retrieveCommentsWithAuthors: PropTypes.func.isRequired,
        post: PropTypes.shape({
            id: PropTypes.number.isRequired,
            url: PropTypes.string.isRequired,
            user: PropTypes.string.isRequired,
            title: PropTypes.string.isRequired,
            content: PropTypes.string.isRequired,
            comments: PropTypes.arrayOf(PropTypes.string.isRequired).isRequired,
            created_at: PropTypes.string.isRequired,
            updated_at: PropTypes.string.isRequired,
            status: PropTypes.string.isRequired
        }).isRequired,
        comments: PropTypes.shape({
            count: PropTypes.number.isRequired,
            next: PropTypes.string,
            previous: PropTypes.string,
            results: PropTypes.arrayOf(
                PropTypes.shape({
                    url: PropTypes.string.isRequired,
                    user: PropTypes.string.isRequired,
                    post: PropTypes.string.isRequired,
                    content: PropTypes.string.isRequired,
                    created_at: PropTypes.string.isRequired,
                    updated_at: PropTypes.string.isRequired
                }).isRequired
            ).isRequired,
            status: PropTypes.string.isRequired
        }).isRequired,
        author: PropTypes.shape({
            count: PropTypes.number.isRequired,
            next: PropTypes.string,
            previous: PropTypes.string,
            results: PropTypes.arrayOf(
                PropTypes.shape({
                    id: PropTypes.number.isRequired,
                    url: PropTypes.string.isRequired,
                    username: PropTypes.string.isRequired,
                    email: PropTypes.string.isRequired,
                    first_name: PropTypes.string.isRequired,
                    last_name: PropTypes.string.isRequired,
                    avatar: PropTypes.string, // TODO: change to isRequired when the avatars are implemented
                    posts: PropTypes.array.isRequired,
                    comments: PropTypes.array.isRequired,
                    is_staff: PropTypes.bool.isRequired,
                    date_joined: PropTypes.string.isRequired,
                    date_updated: PropTypes.string.isRequired
                })
            ).isRequired,
            status: PropTypes.string.isRequired
        })
    };

    componentDidMount = () => {
        this.props.retrievePostWithAuthor();
        this.props.retrieveCommentsWithAuthors();
    };

    render = () => (
        <>
            <PostDetails
                post={this.props.post}
                author={this.props.author.results[0]}
            />
            <CommentList
                comments={this.props.comments}
                authors={this.props.authors}
            />
        </>
    );
}

const mapStateToProps = state => ({
    post: state.post.post,
    author: state.user.post,
    comments: state.comment.comments,
    authors: state.user.comments
});

const mapDispatchToProps = (dispatch, ownProps) => ({
    retrievePostWithAuthor: () =>
        dispatch(retrievePostWithAuthor(ownProps.match.params.id)),
    retrieveCommentsWithAuthors: () =>
        dispatch(retrieveCommentsWithAuthors(ownProps.match.params.id))
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ArchiveDetails);
