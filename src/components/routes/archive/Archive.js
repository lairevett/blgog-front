import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { retrievePostsWithAuthors } from '../../../actions/Post';
import { Container, Header, Body } from '../../layout/generics/Content';
import PostTable from '../../app/post/PostTable';

class Archive extends PureComponent {
    static propTypes = {
        retrievePostsWithAuthors: PropTypes.func.isRequired,
        posts: PropTypes.shape({
            count: PropTypes.number.isRequired,
            next: PropTypes.string,
            previous: PropTypes.string,
            results: PropTypes.arrayOf(
                PropTypes.shape({
                    id: PropTypes.number.isRequired,
                    url: PropTypes.string.isRequired,
                    user: PropTypes.string.isRequired,
                    title: PropTypes.string.isRequired,
                    content: PropTypes.string.isRequired,
                    comments: PropTypes.array.isRequired,
                    created_at: PropTypes.string.isRequired,
                    updated_at: PropTypes.string.isRequired
                }).isRequired
            ).isRequired,
            status: PropTypes.string.isRequired
        }).isRequired,
        authors: PropTypes.arrayOf(
            PropTypes.shape({
                count: PropTypes.number.isRequired,
                next: PropTypes.string,
                previous: PropTypes.string,
                results: PropTypes.arrayOf(
                    PropTypes.shape({
                        id: PropTypes.number.isRequired,
                        url: PropTypes.string.isRequired,
                        username: PropTypes.string.isRequired,
                        email: PropTypes.string.isRequired,
                        first_name: PropTypes.string.isRequired,
                        last_name: PropTypes.string.isRequired,
                        avatar: PropTypes.string, // TODO: change to isRequired when the avatars are implemented
                        posts: PropTypes.array.isRequired,
                        comments: PropTypes.array.isRequired,
                        is_staff: PropTypes.bool.isRequired,
                        date_joined: PropTypes.string.isRequired,
                        date_updated: PropTypes.string.isRequired
                    }).isRequired
                ).isRequired
            }).isRequired
        ).isRequired
    };

    componentDidMount = () => {
        this.props.retrievePostsWithAuthors();
    };

    render = () => (
        <Container>
            <Header>Archive</Header>
            <Body>
                <PostTable
                    posts={this.props.posts}
                    authors={this.props.authors}
                />
            </Body>
        </Container>
    );
}

const mapStateToProps = state => ({
    posts: state.post.posts,
    authors: state.user.posts
});

const mapDispatchToProps = dispatch => ({
    retrievePostsWithAuthors: () => dispatch(retrievePostsWithAuthors())
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Archive);
