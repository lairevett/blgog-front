import React from 'react';
import { Container, Header, Body } from '../../layout/generics/Content';
import Status from '../generics/Status';
import NoComments from './NoComments';
import Comment from './Comment';

export default ({ comments, authors }) => (
    <Container>
        <Header>Comments</Header>
        <Body>
            <Status
                status={comments.status}
                small
                noResultsComponent={<NoComments />}
            >
                {comments &&
                    comments.results.map(
                        (comment, index) =>
                            authors[index] && (
                                <Comment
                                    key={comment.id}
                                    author={authors[index].results[0]}
                                    content={comment.content}
                                    createdAt={comment.created_at}
                                    updatedAt={comment.updated_at}
                                />
                            )
                    )}
            </Status>
        </Body>
    </Container>
);
