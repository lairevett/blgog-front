import React from 'react';
import { Link } from 'react-router-dom';
import { reverse } from 'named-urls';
import { Container, Header, Body, Footer } from '../../layout/generics/Content';
import { routes } from '../../routes/Routes';

export default ({ author, content, createdAt, updatedAt }) => {
    const createdAtConverted = new Date(createdAt);
    const updatedAtConverted = new Date(updatedAt);

    return (
        <Container>
            <Header small>
                <Link to={reverse(routes.aboutDetails.path, { id: author.id })}>
                    {author.username}
                </Link>
            </Header>
            <Body>{content}</Body>
            <Footer>
                Commented on {createdAtConverted.toLocaleDateString()} at{' '}
                {createdAtConverted.toLocaleDateString()}
                {createdAtConverted.getTime() !==
                    updatedAtConverted.getTime() &&
                    `, edited: ${updatedAtConverted.toLocaleString()}`}
            </Footer>
        </Container>
    );
};
