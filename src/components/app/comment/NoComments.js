import React from 'react';
import { Container, Header, Body } from '../../layout/generics/Content';

export default () => (
    <Container>
        <Header small>That's all folks!</Header>
        <Body>
            There aren't any comments on this post yet or the post you're
            looking for is deleted.
        </Body>
    </Container>
);
