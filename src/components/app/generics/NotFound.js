import React from 'react';
import { Container, Header, Body } from '../../layout/generics/Content';

export default () => (
    <Container>
        <Header>Not found!</Header>
        <Body>Look like the page you're looking for wasn't found.</Body>
    </Container>
);
