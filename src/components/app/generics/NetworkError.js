import React from 'react';
import { Container, Header, Body } from '../../layout/generics/Content';

export default ({ small }) => (
    <Container>
        <Header small={small}>Network error!</Header>
        <Body>
            Cannot retrieve resources from the API, try refreshing again later.
        </Body>
    </Container>
);
