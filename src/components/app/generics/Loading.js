import React from 'react';
import { Container, Header, Body } from '../../layout/generics/Content';

export default ({ small }) => (
    <Container>
        <Header small={small}>Loading!</Header>
        <Body>Retrieving resources from API...</Body>
    </Container>
);
