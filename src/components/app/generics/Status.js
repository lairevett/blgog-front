import React from 'react';
import {
    STATUS_LOADING,
    STATUS_NO_RESULTS,
    STATUS_NOT_FOUND,
    STATUS_RETRIEVED,
    STATUS_NETWORK_ERROR
} from '../../../utils/Types';
import Loading from './Loading';
import NotFound from './NotFound';
import NetworkError from './NetworkError';

export default ({ children, status, small, noResultsComponent }) =>
    status === STATUS_LOADING ? (
        <Loading small={small} />
    ) : status === STATUS_NO_RESULTS ? (
        noResultsComponent
    ) : status === STATUS_RETRIEVED ? (
        children
    ) : status === STATUS_NETWORK_ERROR ? (
        <NetworkError small={small} />
    ) : status === STATUS_NOT_FOUND ? (
        <NotFound small={small} />
    ) : null;
