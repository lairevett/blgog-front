import React from 'react';
import { Form, Field, reduxForm } from 'redux-form';
import { renderField } from '../../layout/generics/Forms';
import { validate, submit } from './SignUpFormValidation';

const SignUpForm = ({ error, submitting, pristine, reset, handleSubmit }) => (
    <Form onSubmit={handleSubmit(submit)}>
        {error
            ? error
            : "Please don't reuse your bank password as I didn't spend much time on security for this app."}
        <Field
            name="email"
            type="email"
            label="email:"
            placeholder="dweller2161@vault-tec.com"
            component={renderField}
        />
        <Field
            name="username"
            type="text"
            label="username:"
            placeholder="vault_dweller"
            component={renderField}
        />
        <Field
            name="firstName"
            type="text"
            label="firstname:"
            placeholder="todd"
            component={renderField}
        />
        <Field
            name="lastName"
            type="text"
            label="lastname:"
            placeholder="howard"
            component={renderField}
        />
        <Field
            name="password"
            type="password"
            label="password:"
            placeholder="***********"
            component={renderField}
        />
        <Field
            name="repeatPassword"
            type="password"
            label="repeatpassword:"
            placeholder="***********"
            component={renderField}
        />
        <fieldset>
            <button
                type="reset"
                onClick={reset}
                disabled={pristine || submitting}
            >
                Reset
            </button>
            <button type="submit" disabled={submitting}>
                Create
            </button>
        </fieldset>
    </Form>
);

export default reduxForm({
    form: 'signUpForm',
    validate
})(SignUpForm);
