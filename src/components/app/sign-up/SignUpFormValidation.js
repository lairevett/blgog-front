// import { SubmissionError } from 'redux-form';

// TODO: lift the validation to its own classes to reuse in sign in
export const validate = values => {
    const errors = {};

    if (!values.email) {
        errors.email = 'required';
    } else if (values.email.length > 65) {
        errors.email = 'less than 64 characters';
    } else if (
        !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)
    ) {
        errors.email = 'invalid';
    }

    if (!values.username) {
        errors.username = 'required';
    } else if (values.username.length > 15) {
        errors.username = 'less than 16 characters';
    }

    if (!values.firstName) {
        errors.firstName = 'required';
    } else if (values.firstName.length > 25) {
        errors.firstName = 'less than 24 characters';
    }

    if (!values.lastName) {
        errors.lastName = 'required';
    } else if (values.lastName.length > 25) {
        errors.lastName = 'less than 24 characters';
    }

    if (!values.password) {
        errors.password = 'required';
    } else if (values.password.length > 65) {
        errors.password = 'less than 64 characters';
    }

    if (!values.repeatPassword) {
        errors.repeatPassword = 'required';
    } else if (values.repeatPassword.length > 65) {
        errors.repeatPassword = 'less than 64 characters';
    }

    return errors;
};

export const submit = values => {
    // TODO:
    // if (usernameExists(values.username)) {
    //     throw new SubmissionError({
    //         _error: 'Username is already taken.';
    //     });
    // } else {
    //     create request
    // }
};
