import { SubmissionError } from 'redux-form';

export const validate = values => {
    const errors = {};
    if (!values.username) {
        errors.username = 'required';
    } else if (values.username.length > 16) {
        errors.username = 'must be 16 characters or less';
    }

    if (!values.password) {
        errors.password = 'required';
    }
    return errors;
};

export const submit = values => {
    // TODO: query API for the username or just send authentication string with headers and parse the response
    if (['dummytest'].includes(values.username)) {
        throw new SubmissionError({
            //username: 'user does not exist',
            _error: 'Invalid credentials.'
        });
    } else {
        console.log('submitted');
        // TODO: add authentication string to localStorage API, so the user is logged in when browsing other pages
    }
};
