import React from 'react';
import { Form, Field, reduxForm } from 'redux-form';
import { renderField } from '../../layout/generics/Forms';
import { validate, submit } from './SignInFormValidation';

const SignInForm = ({ error, submitting, pristine, reset, handleSubmit }) => (
    <Form onSubmit={handleSubmit(submit)}>
        <fieldset>
            {error
                ? error
                : 'Welcome, please provide valid credentials below to sign in.'}
        </fieldset>
        <Field
            name="username"
            type="text"
            component={renderField}
            label="username:"
            placeholder="vault_dweller"
        />
        <Field
            name="password"
            type="password"
            component={renderField}
            label="password:"
            placeholder="***********"
        />
        <fieldset>
            <button
                type="reset"
                disabled={pristine || submitting}
                onClick={reset}
            >
                Reset
            </button>
            <button type="submit" disabled={submitting}>
                Enter
            </button>
        </fieldset>
    </Form>
);

export default reduxForm({
    form: 'signIn',
    validate
})(SignInForm);
