import React from 'react';
import { Link } from 'react-router-dom';
import Status from '../generics/Status';
import { Container, Header, Body } from '../../layout/generics/Content';
import { UnorderedList, ListItem } from '../../layout/generics/Lists';

export default ({ user, goBack }) => {
    const {
        status,
        username,
        email,
        posts,
        comments,
        is_staff,
        date_joined
    } = user;

    return (
        <Status status={status}>
            <Container>
                <Header>
                    About {username}{' '}
                    <Link to="#" onClick={goBack}>
                        Back
                    </Link>
                </Header>
                <Body>
                    <UnorderedList>
                        <ListItem>
                            <span className="label">Rank: </span>
                            {is_staff ? 'Vault-Tec Staff' : 'Vault-Tec Member'}
                        </ListItem>
                        <ListItem>
                            <span className="label">Email: </span>
                            {email}
                        </ListItem>
                        <ListItem>
                            <span className="label">Posts: </span>
                            {posts.length}
                        </ListItem>
                        <ListItem>
                            <span className="label">Comments: </span>
                            {comments.length}
                        </ListItem>
                        <ListItem>
                            <span className="label">Joined: </span>
                            {new Date(date_joined).toLocaleString()}
                        </ListItem>
                    </UnorderedList>
                </Body>
            </Container>
        </Status>
    );
};
