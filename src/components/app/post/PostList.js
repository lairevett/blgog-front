import React from 'react';
import Status from '../generics/Status';
import NoPosts from './NoPosts';
import Post from './Post';

export default ({ posts, authors }) => (
    <Status status={posts.status} noResultsComponent={<NoPosts />}>
        <>
            {posts.results &&
                posts.results.map(
                    (post, index) =>
                        authors[index] && (
                            <Post
                                key={post.id}
                                id={post.id}
                                title={post.title}
                                content={post.content}
                                author={authors[index].results[0]}
                                created_at={post.created_at}
                                updated_at={post.updated_at}
                                hasTitleLink
                            />
                        )
                )}
        </>
    </Status>
);
