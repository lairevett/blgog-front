import React from 'react';
import { Container, Header, Body } from '../../layout/generics/Content';

export default () => (
    <Container>
        <Header>No posts!</Header>
        <Body>There aren't any posts.</Body>
    </Container>
);
