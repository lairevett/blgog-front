import React from 'react';
import Status from '../generics/Status';
import Post from './Post';

export default ({ post, author }) => (
    <Status status={post.status}>
        <>
            {author && (
                <Post
                    id={post.id}
                    title={post.title}
                    content={post.content}
                    author={author}
                    created_at={post.created_at}
                    updated_at={post.updated_at}
                    details
                />
            )}
        </>
    </Status>
);
