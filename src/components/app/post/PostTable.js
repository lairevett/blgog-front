import React from 'react';
import Status from '../generics/Status';
import NoPosts from './NoPosts';
import { StyledTable, StyledTh } from '../../layout/generics/Tables';
import PostTableRow from './PostTableRow';

export default ({ posts, authors }) => (
    <Status status={posts.status} small noResultsComponent={<NoPosts />}>
        <StyledTable>
            <thead>
                <tr>
                    <StyledTh>Title</StyledTh>
                    <StyledTh>Author</StyledTh>
                    <StyledTh>Creation</StyledTh>
                    <StyledTh>Edition</StyledTh>
                    {/* TODO: Add actions for staff? */}
                </tr>
            </thead>
            <tbody>
                {posts.results &&
                    posts.results.map(
                        (post, index) =>
                            authors[index] && (
                                <PostTableRow
                                    key={post.id}
                                    id={post.id}
                                    title={post.title}
                                    author={authors[index].results[0]}
                                    created_at={post.created_at}
                                    updated_at={post.updated_at}
                                />
                            )
                    )}
            </tbody>
        </StyledTable>
    </Status>
);
