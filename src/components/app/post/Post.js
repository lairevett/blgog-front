import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { goBack } from 'connected-react-router';
import { reverse } from 'named-urls';
import { routes } from '../../routes/Routes';
import { Container, Header, Body, Footer } from '../../layout/generics/Content';

class Post extends PureComponent {
    render = () => {
        const {
            id,
            title,
            content,
            author,
            created_at,
            updated_at,
            details,
            goBack
        } = this.props;

        const createdAtConverted = new Date(created_at);
        const updatedAtConverted = new Date(updated_at);

        return (
            <Container>
                <Header>
                    {!details ? (
                        <Link to={reverse(routes.archiveDetails.path, { id })}>
                            {title}
                        </Link>
                    ) : (
                        <>
                            {title}{' '}
                            <Link to="#" onClick={goBack}>
                                Back
                            </Link>
                        </>
                    )}
                </Header>
                <Body>
                    {!details ? content.substr(0, 256) : content}
                    {content.length > 256 && !details && '(...)'}
                </Body>
                <Footer>
                    Created by{' '}
                    <Link
                        to={reverse(routes.aboutDetails.path, {
                            id: author.id
                        })}
                    >
                        {author.username}
                    </Link>{' '}
                    on {createdAtConverted.toLocaleDateString()} at{' '}
                    {createdAtConverted.toLocaleTimeString()}
                    {createdAtConverted.getTime() !==
                        updatedAtConverted.getTime() &&
                        `, edited: ${updatedAtConverted.toLocaleString()}`}
                </Footer>
            </Container>
        );
    };
}

const mapDispatchToProps = dispatch => ({
    goBack: () => dispatch(goBack())
});

export default connect(
    null,
    mapDispatchToProps
)(Post);
