import React from 'react';
import { Link } from 'react-router-dom';
import { reverse } from 'named-urls';
import { StyledTd } from '../../layout/generics/Tables';
import { routes } from '../../routes/Routes';

export default ({ id, title, author, created_at, updated_at }) => {
    const createdAtConverted = new Date(created_at);
    const updatedAtConverted = new Date(updated_at);

    return (
        <tr>
            <StyledTd>
                <Link to={reverse(routes.archiveDetails.path, { id })}>
                    {title}
                </Link>
            </StyledTd>
            <StyledTd>
                <Link
                    to={reverse(routes.aboutDetails.path, {
                        id: author.id
                    })}
                >
                    {author.username}
                </Link>
            </StyledTd>
            <StyledTd>{createdAtConverted.toLocaleString()}</StyledTd>
            <StyledTd>
                {createdAtConverted.getTime() !== updatedAtConverted.getTime()
                    ? updatedAtConverted.toLocaleString()
                    : 'Not edited'}
            </StyledTd>
        </tr>
    );
};
