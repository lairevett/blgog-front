import { createBrowserHistory } from 'history';
import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension/developmentOnly';
import { routerMiddleware } from 'connected-react-router';
import thunk from 'redux-thunk';
import createRootReducer from '../reducers/Root';

export const history = createBrowserHistory();

// array of middlewares to apply
const middleware = [
    routerMiddleware(history), // root reducer with router state
    thunk
];

// create redux store with redux devtools attached and middleware
export default initialState =>
    createStore(
        createRootReducer(history),
        initialState,
        composeWithDevTools(applyMiddleware(...middleware))
    );
