export const STATUS_INITIALIZED = '@@status/INITIALIZED';
export const STATUS_LOADING = '@@status/LOADING';
export const STATUS_RETRIEVED = '@@status/RETRIEVED';
export const STATUS_UPDATED = '@@status/UPDATED';
export const STATUS_CREATED = '@@status/CREATED';
export const STATUS_DESTROYED = '@@status/DESTROYED';
export const STATUS_NO_RESULTS = '@@status/NO_RESULTS';
export const STATUS_NOT_FOUND = '@@status/NOT_FOUND';
export const STATUS_NETWORK_ERROR = '@@status/NETWORK_ERROR';

export const POST_RETRIEVE_MULTIPLE = '@@post/RETRIEVE_MULTIPLE';
export const POST_RETRIEVE_ONE = '@@post/RETRIEVE_ONE';
export const POST_CREATE = '@@post/CREATE';
export const POST_UPDATE = '@@post/UPDATE';
export const POST_DESTROY = '@@post/DESTROY';

export const COMMENT_RETRIEVE_MULTIPLE = '@@comment/RETRIEVE_MULTIPLE';
export const COMMENT_CREATE = '@@comment/CREATE';
export const COMMENT_UPDATE = '@@comment/UPDATE';
export const COMMENT_DESTROY = '@@comment/DESTROY';

export const USER_RETRIEVE_ONE_USERNAME = '@@user/RETRIEVE_ONE_USERNAME';
export const USER_RETRIEVE_ONE_ID = '@@user/RETRIEVE_ONE_ID';
export const USER_RETRIEVE_POST_AUTHOR = '@@user/RETRIEVE_POST_AUTHOR';
export const USER_RETRIEVE_POSTS_AUTHORS = '@@user/RETRIEVE_POSTS_AUTHORS';
export const USER_RETRIEVE_COMMENTS_AUTHORS =
    '@@user/RETRIEVE_COMMENTS_AUTHORS';
export const USER_CREATE = '@@user/CREATE';
export const USER_UPDATE = '@@user/UPDATE';
export const USER_DESTROY = '@@user/DESTROY';
