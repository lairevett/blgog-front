import { generateResourceUrlFromId } from '../actions/Generics';

export const RESOURCES = 'https://blgog-django-rest-api.herokuapp.com/api/';
export const POST_RESOURCE = `${RESOURCES}posts/`;
export const COMMENT_RESOURCE = `${RESOURCES}comments/`;
export const USER_RESOURCE = `${RESOURCES}users/`;

export const getPostResourceFromId = id =>
    generateResourceUrlFromId(POST_RESOURCE, id);

export const getCommentsResourceFromPostId = id =>
    `${getPostResourceFromId(id)}comments/`;

export const getUserResourceFromUsername = username =>
    `${USER_RESOURCE}?username=${username}`;

export const getUserResourceFromId = id => `${USER_RESOURCE}${id}`;

export const getUserResourceFromPostId = id =>
    `${getPostResourceFromId(id)}user/`;

export const getUserResourceFromCommentId = id =>
    `${COMMENT_RESOURCE}${id}/user/`;

export const getUserResourcesFromPostIds = ids => {
    const resources = [];
    ids.forEach(id => {
        resources.push(getUserResourceFromPostId(id));
    });
    return resources;
};

export const getUserResourcesFromCommentIds = ids => {
    const resources = [];
    ids.forEach(id => {
        resources.push(getUserResourceFromCommentId(id));
    });
    return resources;
};
