import React from 'react';
import ReactDOM from 'react-dom';
import configureStore, { history } from './utils/Store';
import { Provider } from 'react-redux';
import App from './components/App';

// create store
const store = configureStore(/* any initial preloaded state */);

// render any jsx to root element in public/index.html
ReactDOM.render(
    <Provider store={store}>
        <App history={history} />
    </Provider>,
    document.getElementById('root')
);
